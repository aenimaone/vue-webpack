import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    id: 1,
    name: 'test'
  },
  mutations: {
    addNote: (state, obj) => {
      state.id = obj.id;
      state.name = obj.name
    }
  }
});
