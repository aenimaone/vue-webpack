import Vue from 'vue';
import Router from 'vue-router';
import Foo from '../components/foo.vue';
import Bar from '../components/bar.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {path: '/foo', name: 'Foo', component: Foo},
    {path: '/bar', name: 'Bar', component: Bar}
  ],
});
